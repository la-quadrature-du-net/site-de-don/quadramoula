# SITE DE DONS

## CAHIER DES CHARGES

### 1 – PRÉSENTATION DES ATTENTES 

#### Rôle du site

Le site de soutien sert à collecter les dons individuels pour la Quadrature du Net.

Il est actif tout au long de l'année, mais avec un gros pic de charge durant la campagne de dons officielle, de mi-novembre au 31 décembre.

L’association attend du site :
- qu’il enregistre les dons par CB, ponctuels ou mensuels, et oriente les gens qui veulent faire un don par chèque ou par virement bancaire
- qu’il permette de créer un compte de donateur pour constituer un historique et un cumul de ses dons
- qu’il permette de commander et d’envoyer des contreparties en fonction de certain paliers de cumuls de dons
- qu’il donne une certaine autonomie aux donateur·ices : faire un don, commander des contreparties, résilier un don, modifier un don, modifier ses données (pseudo, e-mail et adresse postale), supprimer son compte.

Les donateur·ices attendent du site :
- qu’il permette de faire un don anonyme
- qu’il permette de faire un don mensuel avec cumul des dons
- qu’il permette de savoir si le don est bien enregistré
- qu’il permette de suivre son cumul et de commander des contreparties
- qu’il permette de voir les données détenues par le site, et les modifier ou les supprimer
- qu’il permette de modifier ou résilier un don mensuel (aujourd’hui c’est impossible)

#### Données personnelles utilisées

Au moment de faire un don, les donateur·ices indiquent à LQDN :
- une adresse e-mail (obligatoire)
- un nom/pseudo (facultatif)
- ces infos sont utilisées pour créer leur page perso et fédérer leurs différents dons (ponctuels ou mensuels) pour qu'ils puissent les cumuler et commander des contreparties
- des infos bancaires, qui restent sur le site de la banque : le site de don ne conserve que des numéros d’identification des transactions
- une adresse postale éphémère pour l’envoi des contreparties.

#### Structure sommaire du site

Page de don et procédure de don
- c’est là qu’on vient faire un don et se connecter à son compte personnel
- la page renvoie à d’autres pages annexes, comme la FAQ, un article sur les finances de l’association, ou le site principal de La Quadrature.

Page personnelle des donateur·ices
- chaque donateur·ice peut se connecter à une page personnelle où son réunies toutes les informations liées à ses dons : mail, identifiant, mot de passe, historique, cumul, commande de contreparties, adresse postale, etc.
- les donateur·ices doivent pouvoir effectuer un certain nombre de modifications : mail, nom, adresse, éventuellement modifier un don mensuel si l’API du site bancaire le permet.

#### Pages d’administration pour l’association

- une page qui recense les dons par ordre chronologique (les plus récents en haut de la page) ; avec la possibilité de filtrer par dates ou par mail/pseudo de donateur·ice, de supprimer et d’ajouter des dons (champ de saisie manuelle et interface d’import de CSV)
- une page pour recenser les donateur·ices, filtrage par e-mail, nom, adresse postale, date des dons
- une page pour recenser les demandes de contreparties, filtrage par date, nature, donateur·ice, nom, adresse

#### Interactions quotidiennes

Principales utilisations du site au quotidien :
- relever les dons du jour, et les exporter par copier-coller
- exporter les demandes de contreparties, par copier-coller ou par CSV
- saisir les dons par virement SEPA ou par chèque dans le compte des donateur·ices
- envoyer des contreparties : vérification des demandes, des noms, des adresses, saisie des demandes envoyées, suppression des adresses, mise à jour automatique des cumuls et du crédit disponible pour demander des contreparties
- résilier un don mensuel par CB (aujourd’hui c’est impossible)
- modifier un don mensuel par CB (aujourd’hui c’est impossible)
- fusionner deux comptes avec leur historique et leur cumul 
- saisir des dons faits par virement SEPA
- supprimer un compte de donateur·ice et les données personnelles associées (aujourd’hui c’est impossible, sauf en bricolant à la main)
- il existait (toujours?) un robot pour fusionner deux comptes avec un nom/pseudo différent qui utilisent la même adresse mail (le problème disparaît si on supprime le pseudo dans le nouveau site pour tout unifier avec l’e-mail)
- importer un fois par mois mois les dons mensuels à partir d’un CSV généré par le site bancaire.


#### Autres attentes

Le site doit être disponible en tant que logiciel libre et open source à destination des associations et autres organisations qui auraient besoin d’un outil similaire pour leurs campagnes de dons.


### 2 – INTERFACE : DU CÔTÉ DES DONATEUR·ICES


#### Page d’accueil

Zone « Faire un don »
- 5 boutons avec des montants : 11 – 42 – 64 – 128 – 314
- champ « Montant libre »
- 2 boutons radio : « Don ponctuel » / « Don mensuel » — avec « Don ponctuel » sélectionné par défaut
- bouton « Faire un don » pour valider le montant sélectionné ou saisi dans le champ
- texte : « Vous pouvez donner par carte bancaire, par chèque ou par virement » [liens vers la FAQ pour « chèque » et « virement »]
- texte : « Les dons à La Quadrature du Net n’étant pas déductibles des impôts, l’association ne peut pas vous fournir de reçu fiscal pour vos dons. Pour en savoir plus, vous pouvez lire cet article sur le financement de l’association. » [lien vers la FAQ sous « déductibles des impôts » et lien sous « cet article » vers l’article : https://www.laquadrature.net/2022/01/14/ni-subventions-ni-deductibilite-fiscale-la-quadrature-ne-tient-presque-que-par-vos-dons/]. 



Zone « Contreparties »
- texte : « En remerciement de votre don vous pouvez recevoir : un pibag (cumul de 64 €) + un pishirt (cumul de 128 €) + un hoopie (cumul de 314 €) »
- pictos des contreparties : voir avec Marne.



Zone de connexion
- lien « Se connecter » : il ouvre une page ou une fenêtre proposant l’alternative habituelle entre se connecter et créer un compte,
- lien « Créer un compte », qui ouvre une page ou une fenêtre pour créer un compte.



Annexes de la page
- texte en pied de page : « Nous gardons seulement les données nécessaires pour votre historique de dons : votre adresse e-mail et le nom ou le pseudo que vous avez choisi. Le cas échéant, votre adresse postale est supprimée après l’envoi de vos contreparties. Vous pouvez faire un don sans créer de compte, mais alors vous ne pourrez pas constituer un historique de dons et demander des contreparties. Vous pouvez demandez la consultation ou la suppression de vos données en écrivant à contact@laquadrature.net. »
- lien vers la FAQ : à mettre en valeur avec un lien texte du genre « Vous avez des questions ? »
- lien vers le site de La Quadrature du Net : une fois en lien texte et une fois sous le logo de l’asso
- lien vers l’adresse de contact : contac@laquadrature.net
- lien vers le dépôt Git de LQDN.

#### Page de FAQ
Elle doit répondre aux principales questions :
- Est-ce que les dons à La Quadrature donnent droit à une déduction fiscale ?
- Est-ce que je peux avoir un reçu de don ?
- J'ai mis en place un don mensuel, comment le modifier ?
- Comment arrêter mon don mensuel ?
- Est-ce que je peux cumuler mes dons à La Quadrature du Net ?
- Comment changer mon e-mail de donateur·trice ?
- Puis-je faire un don par chèque ?
- Puis-je faire un don par virement ?
- Puis-je faire un don en Bitcoin ?
- Puis-je faire un don via Paypal ?
- Peut-on vous soutenir en achetant des t-shirts de La Quadrature du Net ?
- Comment faire pour demander mes contreparties ?
- Quand recevrai-je mon t-shirt ?
- J'avais déjà établi un don régulier via FDNN, est-ce-que je dois faire quelque chose ?
- Mon entreprise peut-elle faire un don ?
- Qui finance La Quadrature du Net ?
- Comment sont traitées mes données ? 

### Procédure de don

Procédure telle qu’elle est aujourd’hui :

- sélectionner un montant :
    - soit en cliquant sur un des boutons de montant prédéfini ;
    - soit en saisissant un montant dans le champ libre ;
- sélectionner la fréquence du don : ponctuel ou mensuel,
- cliquer sur le bouton « Faire un don ».


- étape de récapitulation et de confirmation : possibilité de modifier, cliquer sur « Je valide »


- étape d’identification : saisie du pseudo, du mail, du mot de passe, possibilité de redemander un mot de passe oublié, bouton « Valider » ;
    Texte : « Si vous avez déjà donné, utilisez votre adresse e-mail et votre mot de passe habituel. Vous ne souhaitez pas de compte ; c'est possible, mettez une adresse mail bidon sans mot de passe. »


- nouvelle étape de récapitulation : « Vous allez effectuer un don de XX €. En cliquant sur le bouton ci-dessous, vous allez être redirigé vers le système de paiement de notre banque. », cliquer sur « Valider »

- passage à l’interface de paiement de Systempay (Bred) :

- après enregistrement du don, un mail est envoyé : [voir avec nono comment modifier le texte du mail, je n’y ai pas accès].

### Corrections et améliorations possibles :

- harmoniser les boutons (Je valide/Valider),
- supprimer le champ pseudo : la connexion se fait avec le couple e-mail-mot de passe, et le pseudo était utilisé,
- donner explicitement la possibilité de faire un don anonyme, sans créer de compte de donateur·ice, donc sans cumul ni contreparties possibles.

### Compte de donateur·ice

#### Fonctions attendues et modifications par rapport à la situation actuelle

- se connecter : avec le couple e-mail-mot de passe (supprimer le champ pseudo) ;
    
- mot de passe oublié : aujourd’hui un mot de passe aléatoire est généré et envoyé en clair, il faudrait sans doute moderniser et sécuriser ça avec un lien par mail pour redéfinir un nouveau mot de passe personnalisé ;
    
- procédure de connexion : une fois connecté, il faudrait arriver directement dans l’espace personnel : aujourd’hui, on se retrouve sur la même page qu’avant la connexion, ça provoque un sentiment d’échec pour l’utilisateur·ice — la seule différence est très difficile à voir : le lien « Connexion » en haut à droite a été remplacé par un lien « Contreparties » sur lequel il faut cliquer pour arriver dans l’espace personnel, à condition d’avoir une bonne vue ;
    
- identifiants de connexion (dans la rubrique « Compte ») : je me demande si on a vraiment besoin de demande un pseudo/un nom — c’est un reste de l’époque où on obtenait un piplôme avec remerciement nominatif, etc., aujourd’hui on ne s’en sert plus ;
    
- adresse et e-mail : très important de garder la fonction de modification de l’adresse et de l’e-mail, peut-être ajouter quelque chose pour signaler à l'utilisateur·ice quand l’adresse est mise à jour (de manière générale, ajouter des signaux pour montrer aux utilisateur·ices que leurs actions sont validées et prises en compte) ;
    
- textes à ajouter :
    - une phrase pour expliquer qu’on n’envoie pas de reçu fiscal ;
    - une phrase sur l’exercice du droit de demande et de suppression des données personnelles.

#### Données personnelles enregistrées
1 — Données personnelles collectées par notre site
Les donateur·ices fournissent : 
- un pseudo ou un nom (facultatif) : il était utilisé pour générer des reçus de don personnalisés, mais ce n’est plus le cas, donc on peut sen passer désormais (sauf lien avec le forum, question posée par ailleurs).
- une adresse e-mail (obligatoire) : elle est utilisée pour unifier les différents dons de l’historique et toutes les données du compte, mais je crois que le site actuel lui accole un numéro identifiant qui est le vrai unificateur du compte, à vérifier.

2 — Données personnelles auxquelles on a accès
Les donateur·ices peuvent fournir : 
- leur nom et leur adresse postale s'ils veulent recevoir des contreparties ;
- des données bancaires de paiement qui sont stockées sur le site de la banque (SystemPay + Bred)
- il n'y a aucun tracker sur notre site de dons, mais le site de la banque stocke l'IP de la transaction.
À ma connaissance c'est tout.
3 – INTERFACE : DU CÔTÉ DE L’ADMINISTRATION

#### Gestion des dons

C’est la page qui permet de suivre au quotidien l’actualité des dons.


- les dons apparaissent en tableau, un par ligne, par ordre chronologique inverse (les derniers en premier) avec les colonnes suivantes :
    
    - numéro de transaction : numéro d’ordre fixé par notre site je crois, pas par celui de la banque ;
        
    - la date et l’heure du don : il faut garder l’heure, c’est utile parfois dans les dialogues avec les donateurs inquiets !
        
    - le nom : info inutile si on base l’identifiant du compte à l’adresse e-mail ; c’est le cas aujourd’hui, les comptes sans nom ni pseudo vont très bien ;
        
    - l’adresse e-mail : elle est cliquable pour afficher la fiche de donateur, une fonction à garder ;
        
    - le montant du don : à noter : le site n’accepte que les sommes rondes en euros, sans décimales, alors que dans la pratique on a des dons par virement SEPA avec des centimes (j’arrondis à l’euro supérieur lors de la saisie manuelle) ; est-il possible de prendre en compte les centimes aussi ?
        
    - l’adresse postale : cette info est inutile ici, on peut supprimer cette colonne ;
        
    - statut : une colonne hybride qui affiche à la fois :
        - la nature du don : mensuel / ponctuel ;
        - la réussite ou non de la transaction : validé / non validé : pas sûr que cette dernière info soit utile, sauf si on considère que cette page fait aussi office de log ; on pourrait imaginer que la page affiche par défaut seulement les dons validés, et qu’on affiche les dons non validés avec un filtre, à la demande ? En tout cas il faut arder le critère ponctuel/mensuel
            
    - actions : autre colonne hybride, avec lien cliquable pour Éditer le don (ne fonctionne pas actuellement) ou changer son statut (le valider ou l’invalider) ; la fonction d’édition est utile quand il s’agit d’un don saisi manuellement (chèque ou virement SEPA), en cas de saisie  erronée, il faut donc garder ou réparer cette fonction ; la fonction de validation est utile si on garde la distinction validé/non validé dans les divers historiques de dons, privés ou publics ; sinon, quand quelqu’un fait un don (non validé) avant d’envoyer son chèque ou son virement, je peux saisir le don au lieu de valider celui qui existe déjà ; en tout cas il manque la fonction de suppression d’un don (en cas de mauvaise saisie, etc.).
        
    - en haut de page, des fonctions de recherche et de filtrage pour trier les dons :
        - par date ou par intervalle entre deux dates,
        - par montant, par statut, par adresse e-mail, etc.

Si on ajoutait des fonctions de recherche qui portent sur toutes les données contenues dans les fiches des donateurs (adresse postale ou nom de famille par exemple) alors on pourrait supprimer la page « Gestion des utilisateurs » (voir plus bas) et gérer laccès aux dons et aux donateur·ices sur une seule page (à voir).

### Fonctions attendues :

- visualiser les dons ;
- les filtrer et les exporter en masse ;
- actions : valider, invalider, supprimer, ajouter, modifier un don ;
- importer les dons mensuels grâce au fichier CSV récupéré sur le site de la banque.

Attention, cette fonction d’importation des dons mensuels existe aujourd’hui dans une page « Gestion banque » qu’il est possible de supprimer. Mais cette fonction doit absolument être conservée. j
Comme la fonction « Ajouter un don », elle doit pouvoir être intégrée à la page « Gestion des dons », soit sous forme de bouton qui ouvre un pop-up, soit en pied de page : il faut seulement un champ de sélection du fichier à importer et un bouton de validation.



#### Ajouter des dons

Cette page n’a qu’une fonction : ajouter un don. Mais c’est une fonction très importante :

- l’essentiel de la base de données est constitué par les dons enregistrés directement sur le site, mais on a toujours besoin d’ajouter des dons à la main : les chèques, les virements SEPA, la fusion manuelle de comptes, le rattrapage de vieux dons mensuels non pris en compte, etc. ;
    
- il faut donc garder une fonction de saisie des dons, avec : un champ e-mail, un champ montant, un bouton valider et un bouton modifier ;
    
- peut-on intégrer cette fonction dans la page « Gestion des dons » ? Les deux champs de saisie (e-mail et montant) peuvent tenir dans un pop-up ou dans un coin de la fenêtre principale, tout en haut ou tout en bas.



#### Gestion des contreparties

C’est la page pour gérer les demandes de contreparties, voici comment elle se présente aujourd’hui :

- les demandes sont affichées dans un tableau, avec une ligne par item (sac, t-shirt, sweat), par ordre chronologique inverse (les dernières demandes sont en haut de la page, c’est très pratique au quotidien pour les repérer facilement) ;
    
- les colonnes du tableau sont les suivantes :
    
    - deux colonnes « ID » et « ID du parent » : apparemment le « parent » désigne l’item le plus élevé (sweat > t-shirt > sac) et les autres s’incrémentent à partir de là – je ne sais pas si le « parent » est utilisé au moment de défalquer le montant de la commande au cumul de dons ;
    - le mail du compte de donateur·ice : cliquable pour arriver sur la fiche de donateur·ice, fonction à garder ;
    - la date de la demande ;
    - l’item demandé ;
    - la taille ;
    - l’adresse postale : dans le site actuelle elle commence par le pseudo ce qui est complètement inutile, il faudrait que ce soit par le nom de donateur·ice ;
    - le statut de la demande : demandé / envoyé / annulé — et aussi NPAI, jamais utilisé : en cas de retour du colis on entre en contact par mail avec la personne demandeuse pour lui renvoyer son colis ;
    - une colonne commentaire jamais utilisée qu’on peut supprimer ;
    - une colonne « Actions » : pour changer le statut de la demande (demandé / envoyé / annulé) ;
        
- en haut de la page, un champ de recherche et des fonctions de filtrage : trier par item demandé, par e-mail, par statut de la demande, et exporter en CSV. Au quotidien comme l’export CSV ne fonctionne pas, je me sers seulement de la recherche par e-mail, et de l’exportation par copier-coller.


### Fonctions à ajouter ou à améliorer :

- export au foramt .csv : la page fonctionne comme une simple base de données : j’exporte les dernières demandes (par copier-coller) dans un fichier local pour gérer les étapes de préparation des colis et les cas particuliers : double demande, adresse incomplète, etc. ; une exportation en .csv serait appréciée (aujourd’hui elle ne fonctionne pas) ;
    
- fonctions de recherche : dans l’idéal il faudrait améliorer les fonctions de recherche : parfois on n’a qu’un bout d’adresse pour retrouver une demande, et ça ne permet pas de retrouver une demande. Dans cette page comme dans la page « Gestion des utilisateurs », il faudrait que  le champ de recherche aille voir en plein texte dans tous les éléments de la base de données, et pas seulement un ou deux par page.

Calcul des cumuls de dons : des fonctions invisibles à ne pas oublier

Attention, derrière la gestion des contreparties il y a forcément deux fonctions de calcul automatiques à prévoir :

- calcul des historiques de dons : c’est la somme de tous les dons validés d’un donateur·ice ;
    
- calcul des cumuls : c’est la somme disponible pour demander des contreparties :
    
    - au départ, c’est la somme des historiques ;
    - mais ensuite il faut déduire des sommes pour chaque demande ;
    - les sommes correspondent à des paliers d’accès aux contreparties :
        - un sac : 64 €
        - un t-shirt + sac : 128 €
        - un sweat + t-shirt + sac : 314  €
    - pour déduire au moment d’une commande, on se base sur le montant associé à la contrepartie la plus chère demandée : si quelqu’un veut le kit complet on lui déduit 314 €, si c’est deux t-shirts alors on lui déduit 2*128 €, etc.

### Gestion des utilisateurs 

C’est la page dans laquelle on retrouve tous les comptes de donateur·ices et les moyens de les explorer.

Pour être franc, la page en tant que telle n’a aucun intérêt : on n’a pas besoin d’une liste des donateur·ices au quotidien. Ce qui est intéressant, ce sont les fonctions de recherche pour retrouver un compte parmi tous les autres.
On pourrait donc envisager d’intégrer ces fonctions de recherches spécifiques dans la page Gestion des dons, comme déjà suggéré pour les fonctions « Ajouter un don » et « Gestion banque » (pour l’import des CSV de la dons mensuels, voir plus haut).

Au quotidien, cette page « Gestion des utilisateurs » est très utile pour retrouver les donateur·ices à partir d’une information partielle : un bout d’e-mail, un bout de nom propre, etc. Il est donc très important que la fonction de recherche aille fouiller dans toutes les données contenues dans un compte de donateur·ice : l’e-mail, le nom d’état civil, l’adresse postale. 
À partir de ces éléments de recherche, on aurait une page de résultats, avec les adresses e-mail des comptes qui sont cliquables pour accéder aux comptes individuels (voir « Fiche individuelle de donateur·ice »). Tout le reste peut disparaître.

### Fiche individuelle de donateur·ice

Quand on clique sur l’adresse e-mail qui unifie et représente un compte de donateur·ice, on arrive sur la fiche individuelle de chaque compte.

C’est une page synthétique qui rassemble :
– un champ nom/pseudo : on peut dégager le pseudo et ne garder que le nom (qui peut être un pseudo)
– un champ pour l’adresse e-mail : avec la possibilité de le modifier ; cette adresse sert d’identité au compte, et elle sert à s’y connecter ; mais aujourd’hui, elle ne sert pas d’identifiant pour la banque, en particulier dans le cas des dons mensuels, ce qui permet de mettre à jour le cumul d’un don mensuel même si l’adresse e-mail a été modifiée ; c’est-à-dire qu’un numéro d’ordre est associé à l’adresse e-mail au moment de la création du compte, et conservé même en cas de changement d’e-mail
– l’historique de tous les dons : dons ponctuels et mensuels qui ont été validés ; on n’a sans doute pas besoin de garder dans l’historique, côté donateur·ice, les échecs et les dons non valides qui créent beaucoup de bruit ; mais il est peut-être sage de les garder côté administration, dans cette fiche individuelle, pour qu’on puisse rassurer les donateur·ices en cas d’inquiétude (« chez nous il n’est pas validé »)
– le montant de l’historique des dons : c’est la somme de tous les dons validés
– le montant du cumul : la somme disponible pour demander des contreparties (voir plus haut dans « Calcul des cumuls de dons : des fonctions invisibles à ne pas oublier »)
– les contreparties demandées : la nature des contreparties demandées, avec la date et leur statut (en attente, expédiées, etc.)
– l’adresse postale d’envoi des contreparties : avec un bouton pour la modifier ; dans l’idéal il faudrait aussi un bouton pour la supprimer ou une fonction de suppression automatique de l’adresse postale quand les contreparties sont notées avec un statut « envoyées » (ldiée étant de miniminser le nombre de données personnelles conservées) ; mais il faut quand même garder le nom d’état civil, au cas où les gens changent d’email ou ne se rappellent plus lequel ils avaient donné.

### Administrateurs
Pour gérer les accès à cette interface d’administration : créer un compte, le supprimer, et changer de mot de passe. C’est très utile, une fonction à garder !

### Gestion des CB
Aujourd’hui cette rubrique ne fonctionne pas du tout. Dans l’idéal, voici les fonctions qu’elle pourrait remplir :
– se connecter au site bancaire et scraper les CB qui arrivent à échéance dans le mois courant
– les lister, avec des « actions » : à relancer, relancé, résilié, etc.
Mais en réalité, c’est loin de la pratique : avant de relancer les donateur·ices il faut un vrai travail de « qualification » des Carets à échéance : parfois un autre don existe déjà, ou le don a été suspendu/annulé côté donateur·ice, etc. Il faut donc travailler plus finement, dans un fichier local, et à partir du back-office de la banque. Cette rubrique peut être supprimée.

### Tableau de bord
Censé donner une vision synoptique de l’état des demandes de contreparties. Mais en réalité jamais utilisé, les chiffres n’ont aucune utilité pratique pour l’envoi des contreparties. Rubrique à supprimer.

### Gestion banque
La plupart des fonctions dans cette page ne sont jamais utilisées :
– « Mise à jour des dates d’expiration des CB » : à supprimer ;
– « Vérification des dons » : à supprimer ;
– « Mise à jour des dons » : c’est la seule qui est utilisée une fois par mois, pour importer le CSV des dons mensuels encaissés le 8 et le 9 du mois.
On peut donc ne conserver que cette dernière fonction. Elle a trois éléments d’UI : un champ de sélection du fichier et un bouton « Importer » (ou « pousser le fichier ») et une case à cocher « Testing ». 
Cette case est cochée par défaut et permet de simuler l’importation du fichier. Mais la site est ainsi fait que ces simulations laissent aujourd’hui des traces redondantes dans les historiques de dons. Cette option peut donc être supprimée sans regret. Si elle est maintenue par prudence (elle permet de détecter de possibles erreurs avant de pousser le fichier pour de vrai), alors s’assurer au moins qu’elle ne laisse pas de dons « fantômes » dans les historiques et les cumuls de dons. 

### Statistiques
Cette page affiche les 30 derniers dons, et la répartition des dons sur les 30 derniers jours. Ces infos sont rassemblées et traitées ailleurs, en-dehors du site, on peu donc supprimer cette page. 