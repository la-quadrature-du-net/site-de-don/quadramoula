import './style/admin/admin.scss';

import $ from 'jquery';
import userCrud from "./script/admin/userCrud";

$(function () {
    userCrud.init();
});