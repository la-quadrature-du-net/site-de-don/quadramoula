import $ from 'jquery';

export default {

    init: function () {
        let t = this;
        t.initFaqAnchorAutoOpen();
    },

    initFaqAnchorAutoOpen: function () {
        if ($('body').hasClass('faq-page') && window.location.hash !== '') {
            $(window.location.hash).closest('.accordion-item').find('.accordion-button').click();
        }
    },
}
