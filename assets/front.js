import './style/front/front.scss';

import $ from 'jquery';
import faq from "./script/front/faq";
import modal from "./script/front/modal";
import userCompensation from "./script/front/userCompensation";
import donationForm from "./script/front/donationForm";

require('@fortawesome/fontawesome-free/css/all.min.css');

$(function () {
    modal.init();
    faq.init();
    userCompensation.init();
    donationForm.init();
});