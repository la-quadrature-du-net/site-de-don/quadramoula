<?php

namespace App\Util;

use App\Entity\Donation;
use App\Entity\UserBankAlias;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

interface PaymentServiceInterface
{
    public function processPaymentRequest(Donation $donation, ?UserBankAlias $userBankAlias): Response;

    public function processPaymentResponse(Request $request, Donation $donation): bool;
}