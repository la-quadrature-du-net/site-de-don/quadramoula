<?php

namespace App\Filter;

use App\Entity\Address;
use App\Entity\User;
use Doctrine\ORM\Query\Expr;
use Doctrine\ORM\QueryBuilder;
use EasyCorp\Bundle\EasyAdminBundle\Contracts\Filter\FilterInterface;
use EasyCorp\Bundle\EasyAdminBundle\Dto\EntityDto;
use EasyCorp\Bundle\EasyAdminBundle\Dto\FieldDto;
use EasyCorp\Bundle\EasyAdminBundle\Dto\FilterDataDto;
use EasyCorp\Bundle\EasyAdminBundle\Filter\FilterTrait;
use EasyCorp\Bundle\EasyAdminBundle\Form\Filter\Type\EntityFilterType;

class CompensationAddressFilter implements FilterInterface
{
    use FilterTrait;

    public static function new(): self
    {
        return (new self())
            ->setFilterFqcn(__CLASS__)
            ->setProperty('address')
            ->setLabel('fields.address')
            ->setFormType(EntityFilterType::class)
            ->setFormTypeOption('value_type_options', ['class' => Address::class]);
    }

    public function apply(QueryBuilder $queryBuilder, FilterDataDto $filterDataDto, ?FieldDto $fieldDto, EntityDto $entityDto): void
    {
        $queryBuilder
            ->innerJoin(User::class, 'u', Expr\Join::WITH, 'entity.user = u')
            ->innerJoin(Address::class, 'a', Expr\Join::WITH, 'a.user = u')
            ->andWhere(sprintf('a %s :%s', $filterDataDto->getComparison(), $filterDataDto->getParameterName()))
            ->setParameter($filterDataDto->getParameterName(), $filterDataDto->getValue());
    }
}