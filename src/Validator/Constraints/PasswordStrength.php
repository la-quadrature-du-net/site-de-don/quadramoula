<?php

namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

#[\Attribute]
class PasswordStrength extends Constraint
{
    public $message;

    public function __construct(string $message)
    {
        parent::__construct();
        $this->message = $message;
    }

    public function validatedBy()
    {
        return static::class . 'Validator';
    }
}
