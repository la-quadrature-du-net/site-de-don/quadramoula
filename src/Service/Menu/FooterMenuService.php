<?php

namespace App\Service\Menu;

use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class FooterMenuService
{
    private UrlGeneratorInterface $router;
    private TranslatorInterface $translator;

    public function __construct(
        UrlGeneratorInterface $router,
        TranslatorInterface   $translator,
    )
    {
        $this->router = $router;
        $this->translator = $translator;
    }

    public function getNavLinks()
    {
        $ts = fn($message) => $this->translator->trans($message);
        $links = [
            [
                'href' => $this->router->generate('faq'),
                'text' => $ts('front.footer.nav.faq.text'),
            ], [
                'href' => $ts('front.footer.nav.lqdn.link'),
                'text' => $ts('front.footer.nav.lqdn.text'),
            ], [
                'href' => $ts('front.footer.nav.git.link'),
                'text' => $ts('front.footer.nav.git.text'),
            ], [
                'href' => $ts('front.footer.nav.contact.link'),
                'text' => $ts('front.footer.nav.contact.text'),
            ],
        ];
        return $links;
    }
}