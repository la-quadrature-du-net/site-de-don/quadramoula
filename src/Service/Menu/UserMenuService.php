<?php

namespace App\Service\Menu;

use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class UserMenuService
{
    private UrlGeneratorInterface $router;
    private TranslatorInterface $translator;
    private $request;

    public function __construct(
        UrlGeneratorInterface $router,
        TranslatorInterface   $translator,
        RequestStack          $requestStack,
        Security              $security,
    )
    {
        $this->router = $router;
        $this->translator = $translator;
        $this->request = $requestStack->getCurrentRequest();
    }

    public function getNavLinks()
    {
        $ts = fn($message) => $this->translator->trans($message);
        $links = [
            [
                'href' => $this->router->generate('user_dashboard'),
                'text' => $ts('front.usernav.links.dashboard'),
                'active' => $this->request->attributes->get('_route') == 'user_dashboard',
                'icon' => 'house',
            ],[
                'href' => $this->router->generate('user_account'),
                'text' => $ts('front.usernav.links.account'),
                'active' => $this->request->attributes->get('_route') == 'user_account',
                'icon' => 'user',
            ],[
                'href' => $this->router->generate('user_address'),
                'text' => $ts('front.usernav.links.address'),
                'active' => $this->request->attributes->get('_route') == 'user_address',
                'icon' => 'location-dot',
            ],[
                'href' => $this->router->generate('user_history'),
                'text' => $ts('front.usernav.links.history'),
                'active' => $this->request->attributes->get('_route') == 'user_history',
                'icon' => 'clock-rotate-left',
            ],[
                'href' => $this->router->generate('user_compensation'),
                'text' => $ts('front.usernav.links.compensation'),
                'active' => $this->request->attributes->get('_route') == 'user_compensation',
                'icon' => 'gift',
            ], [
                'href' => $this->router->generate('user_logout'),
                'text' => $ts('front.usernav.links.logout'),
                'active' => false,
                'icon' => 'right-from-bracket',
            ],
        ];
        return $links;
    }
}