<?php

namespace App\Service\Menu;

use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class TopMenuService
{
    private UrlGeneratorInterface $router;
    private TranslatorInterface $translator;
    private $request;
    private Security $security;

    public function __construct(
        UrlGeneratorInterface $router,
        TranslatorInterface   $translator,
        RequestStack          $requestStack,
        Security              $security,
    )
    {
        $this->router = $router;
        $this->translator = $translator;
        $this->request = $requestStack->getCurrentRequest();
        $this->security = $security;
    }

    public function getNavLinks()
    {
        $ts = fn($message) => $this->translator->trans($message);
        $links = [
            [
                'href' => $this->router->generate('index'),
                'text' => $ts('front.navbar.links.home'),
                'active' => $this->request->attributes->get('_route') == 'index',
            ],
        ];
        if (null !== $this->security->getUser()) {
            $links = array_merge($links, [
                [
                    'href' => $this->router->generate('user_dashboard'),
                    'text' => $ts('front.navbar.links.dashboard'),
                    'active' => in_array($this->request->attributes->get('_route'), ['user_dashboard', 'user_account', 'user_address', 'user_history', 'user_compensation']),
                ], [
                    'href' => $this->router->generate('faq'),
                    'text' => $ts('front.navbar.links.faq'),
                    'active' => $this->request->attributes->get('_route') == 'faq',
                ], [
                    'href' => $this->router->generate('user_logout'),
                    'text' => $ts('front.navbar.links.logout'),
                    'active' => false,
                ],
            ]);
        } else {
            $links = array_merge($links, [
                [
                    'href' => $this->router->generate('faq'),
                    'text' => $ts('front.navbar.links.faq'),
                    'active' => $this->request->attributes->get('_route') == 'faq',
                ], [
                    'href' => $this->router->generate('user_register'),
                    'text' => $ts('front.navbar.links.register'),
                    'active' => $this->request->attributes->get('_route') == 'user_register',
                    'modal' => true,
                ], [
                    'href' => $this->router->generate('user_login'),
                    'text' => $ts('front.navbar.links.login'),
                    'active' => $this->request->attributes->get('_route') == 'user_login',
                    'modal' => true,
                ],
            ]);
        }
        return $links;
    }
}