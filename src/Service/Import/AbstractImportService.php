<?php

namespace App\Service\Import;

use PhpOffice\PhpSpreadsheet\Reader\Csv;

abstract class AbstractImportService
{
    public function getCsvData(string $filePath): array
    {
        $reader = new Csv();
        $spreadsheet = $reader->load($filePath);
        $rawData = $spreadsheet->getActiveSheet()->toArray('');
        $keys = array_map('trim', $rawData[0]);
        $csvData = [];
        for ($i = 1; $i < count($rawData); $i++) {
            $row = array_combine($keys, $rawData[$i]);
            $csvData[] = array_map(function($value) {
                return $this->cleanString($value);
            }, $row);
        }
        return $csvData;
    }

    protected function cleanString($value) {
        $value = trim($value);
        if ($value === '\N') {
            $value = '';
        }
        return $value;
    }
}