<?php

namespace App\Service\Import;

use App\Entity\Donation;
use App\Entity\UserBankAlias;
use App\Repository\DonationRepository;
use App\Repository\UserBankAliasRepository;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;

class BankDonationCsvImportService extends AbstractImportService
{
    public function __construct(private EntityManagerInterface $entityManager, private DonationRepository $donationRepository, private UserBankAliasRepository $userBankAliasRepository)
    {
    }

    public function processImport(array $csvData, bool $test): array
    {
        $results = [];
        foreach ($csvData as $transactionData) {
            $alias = $transactionData['Alias'];
            $transaction = $transactionData['Transaction'];
            $date = DateTime::createFromFormat('d/m/Y H:i:s', $transactionData['Date remise']);
            $amount = (float) $transactionData['Montant du paiement'];
            $subscription = $transactionData['Abonnement'];
            if ($transactionData['Alias'] != '') {
                $results[$alias] = [];
                $userBankAlias = $this->userBankAliasRepository->findOneByBankAlias($alias);
                if (null === $userBankAlias) {
                    $results[$alias]['userBankAlias'] = 'create';
                    if (!$test) {
                        $userBankAlias = new UserBankAlias();
                        $userBankAlias->setBankAlias($alias);
                        $this->entityManager->persist($userBankAlias);
                    }
                } else {
                    $results[$alias]['userBankAlias'] = 'exists';
                }
                $donation = null === $userBankAlias || null === $userBankAlias->getId() ? null : $this->donationRepository->findOneByTransaction($transaction);
                if (null === $donation) {
                    $results[$alias]['donation'] = 'create';
                    if (!$test) {
                        $user = $userBankAlias->getUser();
                        $donation = new Donation();
                        $donation->setUser($user);
                        $donation->setUserBankAlias($userBankAlias);
                        $donation->setTransaction($transaction);
                        $donation->setAmount($amount);
                        $donation->setCreatedAt($date);
                        $donation->setPaymentMethod(Donation::PAYMENT_METHOD_CB);
                        $donation->setStatus(Donation::STATUS_MONTHLY_HANDED);
                        $this->entityManager->persist($donation);
                        if ($userBankAlias->getSubscription() != $subscription) {
                            $userBankAlias->setSubscription($subscription);
                        }
                    }
                } else {
                    $results[$alias]['donation'] = 'exists';
                }
            }
            if (!$test) {
                $this->entityManager->flush();
            }
        }
        return $results;
    }

}