<?php

namespace App\Service\Import;

use App\Entity\Donation;
use App\Entity\User;
use App\Entity\UserBankAlias;
use App\Repository\DonationRepository;
use App\Repository\UserBankAliasRepository;
use App\Repository\UserRepository;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Helper\ProgressBar;

class DonationCsvImportService extends AbstractImportService
{
    public function __construct(
        private EntityManagerInterface  $entityManager,
        private UserRepository          $userRepository,
        private UserBankAliasRepository $userBankAliasRepository,
        private DonationRepository      $donationRepository,
    )
    {
    }

    public function processImport(array $csvData, bool $test, ?ProgressBar $progressBar = null): array
    {
        set_time_limit(0);
        $results = [];
        foreach ($csvData as $i => $donationData) {
            $email = $donationData['email'];
            $status = (int) $donationData['status'];
            $locale = explode('_', $donationData['locale'])[0];
            $amount = (int) $donationData['amount'];
            $paymentMethod = (int) $donationData['payment_method'];
            $date = new DateTime($donationData['date']);
            $bankAlias = $donationData['bank_alias'];
            $transaction = $donationData['transaction'];
            $donation = $this->donationRepository->findOneByTransaction($transaction);
            if (null !== $donation) {
                $results[$i] = 'exists';
            } else {
                $results[$i] = 'create';
                //if (!$test) {
                $user = $this->userRepository->findOneByEmail($email);
                if (null === $user) {
                    $user = new User();
                    $user->setEmail($email);
                    $user->setPassword('');
                    if (!$test) {
                        $this->entityManager->persist($user);
                    }
                }
                $donation = new Donation();
                $donation->setUser($user);
                $donation->setStatus($status);
                $donation->setLocale($locale);
                $donation->setAmount($amount);
                $donation->setPaymentMethod($paymentMethod);
                $donation->setTransaction($transaction);
                $donation->setUserTotalCumulUpdated(true);
                $donation->setCreatedAt($date);
                if (!$test) {
                    $this->entityManager->persist($donation);
                }
                if ($bankAlias != '') {
                    $userBankAlias = $this->userBankAliasRepository->findOneByBankAlias($bankAlias);
                    if (null === $userBankAlias) {
                        $userBankAlias = new UserBankAlias();
                        $userBankAlias->setUser($user);
                        $userBankAlias->setBankAlias($bankAlias);
                        if (!$test) {
                            $this->entityManager->persist($userBankAlias);
                        }
                        $donation->setUserBankAlias($userBankAlias);
                    }
                }
                //}
            }
            if (!$test) {
                $this->entityManager->flush();
                $this->entityManager->clear();
            }
            if (null !== $progressBar) {
                $progressBar->advance();
            }
        }
        return $results;
    }
}