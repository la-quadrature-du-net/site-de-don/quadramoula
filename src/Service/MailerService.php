<?php

namespace App\Service;

use App\Entity\EmailLog;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Part\DataPart;
use Symfony\Component\Mime\Part\File;
use Twig\Environment;

class MailerService
{
    public function __construct(
        private MailerInterface        $mailer,
        private EntityManagerInterface $entityManager,
        private Environment            $twig)
    {
    }

    public function sendEmail(int $type, string $subject, array $toEmails, string $templateName, array $context = [], $attachment = null): void
    {
        $email = (new TemplatedEmail())
            ->to(...$toEmails)
            ->subject($subject)
            ->htmlTemplate('emails/' . $templateName . '.html.twig')
            ->context($context);
        if ($attachment !== null) {
            $email->addPart(new DataPart(new File($attachment)));
        }
        $this->logEmail($type, $email, $templateName, $context);
        $this->mailer->send($email);
    }

    public function logEmail(int $type, TemplatedEmail $email, string $templateName, array $context = [])
    {
        $log = new EmailLog();
        $log->setType($type);
        $log->setSender($_ENV['FROM_EMAIL']);
        $recipients = [];
        foreach ($email->getTo() as $to) {
            $recipients[] = $to->getAddress();
        }
        $log->setRecipient(implode(';', $recipients));
        $log->setSubject($email->getSubject());
        $log->setBody($this->twig->render('emails/' . $templateName . '.html.twig', $context));
        $log->setSentAt(new DateTime('now'));
        $this->entityManager->persist($log);
        $this->entityManager->flush();
    }
}