<?php

namespace App\EventListener;

use App\Entity\Gift;
use Doctrine\Bundle\DoctrineBundle\Attribute\AsEntityListener;
use Doctrine\ORM\Event\PreRemoveEventArgs;
use Doctrine\ORM\Events;

#[AsEntityListener(event: Events::preRemove, method: 'preRemove', entity: Gift::class)]
class GiftListener
{
    public function preRemove(Gift $gift, PreRemoveEventArgs $args): void
    {
        foreach ($gift->getCompensations() as $compensation) {
            $compensation->setGift(null);
        }
    }
}