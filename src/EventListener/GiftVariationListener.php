<?php

namespace App\EventListener;

use App\Entity\GiftVariation;
use Doctrine\Bundle\DoctrineBundle\Attribute\AsEntityListener;
use Doctrine\ORM\Event\PreRemoveEventArgs;
use Doctrine\ORM\Events;

#[AsEntityListener(event: Events::preRemove, method: 'preRemove', entity: GiftVariation::class)]
class GiftVariationListener
{
    public function preRemove(GiftVariation $giftVariation, PreRemoveEventArgs $args): void
    {
        foreach ($giftVariation->getCompensations() as $compensation) {
            $compensation->setGiftVariation(null);
        }
    }
}