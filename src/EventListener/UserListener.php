<?php

namespace App\EventListener;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Attribute\AsEntityListener;
use Doctrine\ORM\Event\PreRemoveEventArgs;
use Doctrine\ORM\Events;

#[AsEntityListener(event: Events::preRemove, method: 'preRemove', entity: User::class)]
class UserListener
{
    public function preRemove(User $user, PreRemoveEventArgs $args): void
    {
        foreach ($user->getDonations() as $donation) {
            $donation->setUser(null);
        }
        foreach ($user->getCompensations() as $compensation) {
            $compensation->setUser(null);
        }
    }
}