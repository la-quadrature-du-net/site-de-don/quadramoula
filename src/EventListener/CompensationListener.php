<?php

namespace App\EventListener;

use App\Entity\Compensation;
use Doctrine\Bundle\DoctrineBundle\Attribute\AsEntityListener;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Event\PostUpdateEventArgs;
use Doctrine\ORM\Events;

#[AsEntityListener(event: Events::postUpdate, method: 'postUpdate', entity: Compensation::class)]
class CompensationListener
{
    public function __construct(private EntityManagerInterface $entityManager)
    {
    }

    public function postUpdate(Compensation $compensation, PostUpdateEventArgs $args): void
    {
        $this->maybeDeleteUserAddress($compensation);
    }

    public function maybeDeleteUserAddress(Compensation $compensation)
    {
        if (
            $compensation->getStatus() == Compensation::STATUS_SENT
            && !$compensation->isKeepAddress()
            && ($user = $compensation->getUser())
            && ($address = $user->getAddress())
        ) {
            $user->setAddress(null);
            $this->entityManager->remove($address);
            $this->entityManager->flush();
        }
    }
}