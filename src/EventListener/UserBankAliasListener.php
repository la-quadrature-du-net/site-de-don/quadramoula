<?php

namespace App\EventListener;

use App\Entity\UserBankAlias;
use Doctrine\Bundle\DoctrineBundle\Attribute\AsEntityListener;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Event\PreRemoveEventArgs;
use Doctrine\ORM\Events;

#[AsEntityListener(event: Events::preRemove, method: 'preRemove', entity: UserBankAlias::class)]
class UserBankAliasListener
{
    public function preRemove(UserBankAlias $userBankAlias, PreRemoveEventArgs $args): void
    {
        foreach ($userBankAlias->getDonations() as $donation) {
            $donation->setUserBankAlias(null);
        }
    }
}