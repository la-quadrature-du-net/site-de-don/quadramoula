<?php

namespace App\Command;

use App\DataFixtures\Providers\HashPasswordProvider;
use App\Entity\AdminUser;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

#[AsCommand('app:make:adminuser')]
class MakeAdminUserCommand extends AbstractCommand
{
    private $username;
    private $email;
    private $lastname;
    private $firstname;
    private $password;
    private $error = false;

    public function __construct(
        private HashPasswordProvider   $passwordEncoder,
        private EntityManagerInterface $entityManager,
        private TranslatorInterface    $translator)
    {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->addOption('username', null, InputOption::VALUE_OPTIONAL)
            ->addOption('email', null, InputOption::VALUE_OPTIONAL)
            ->addOption('firstname', null, InputOption::VALUE_OPTIONAL)
            ->addOption('lastname', null, InputOption::VALUE_OPTIONAL)
            ->addOption('password', null, InputOption::VALUE_OPTIONAL);
    }

    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        $this->username = $input->getOption('username');
        $this->email = $input->getOption('email');
        $this->firstname = $input->getOption('firstname') ?: '';
        $this->lastname = $input->getOption('lastname') ?: '';
        $this->password = $input->getOption('password');
        if (!$input->isInteractive()) {
            if (null == $this->username) {
                $output->writeln($this->translator->trans('make_admin_user_command.error.is_mandatory', ['%parameter%' => 'username']));
                $this->error = true;
            }
            if (null == $this->email) {
                $output->writeln($this->translator->trans('make_admin_user_command.error.is_mandatory', ['%parameter%' => 'email']));
                $this->error = true;
            }
            if (null == $this->password) {
                $output->writeln($this->translator->trans('make_admin_user_command.error.is_mandatory', ['%parameter%' => 'password']));
                $this->error = true;
            }
        }
    }

    protected function interact(InputInterface $input, OutputInterface $output)
    {
        $ts = fn($message) => $this->translator->trans($message);
        $this->username = $this->askQuestion($ts('fields.username') . ' : ', $input, $output, $this->username);
        $this->email = $this->askQuestion($ts('fields.email') . ' : ', $input, $output, $this->email);
        $this->firstname = $this->askQuestion($ts('fields.firstname') . ' : ', $input, $output, $this->firstname);
        $this->lastname = $this->askQuestion($ts('fields.lastname') . ' : ', $input, $output, $this->lastname);
        $this->password = $this->askHiddenQuestion($ts('fields.password') . ' : ', $input, $output, $this->password);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if ($this->error) {
            return Command::FAILURE;
        }

        $adminUser = new AdminUser();
        $adminUser->setUsername($this->username);
        $adminUser->setPassword($this->passwordEncoder->hashPassword($this->password));
        $adminUser->setEmail($this->email);
        $adminUser->setLastName($this->lastname);
        $adminUser->setFirstName($this->firstname);

        $this->entityManager->persist($adminUser);
        $this->entityManager->flush();

        $output->writeln($this->translator->trans('make_admin_user_command.admin_user_created'));

        return Command::SUCCESS;
    }
}