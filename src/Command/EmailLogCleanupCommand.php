<?php

namespace App\Command;

use App\Repository\EmailLogRepository;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Contracts\Translation\TranslatorInterface;

#[AsCommand('app:email:log:cleanup')]
class EmailLogCleanupCommand extends AbstractCommand
{
    public function __construct(
        private EmailLogRepository  $emailLogRepository,
        private TranslatorInterface $translator)
    {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $count = $this->emailLogRepository->deleteOld();
        $io->success(sprintf($this->translator->trans('email_log_cleanup_command.old_log_emails_deleted') . ' : %d', $count));

        return Command::SUCCESS;
    }
}