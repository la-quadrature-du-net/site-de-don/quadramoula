<?php

namespace App\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

abstract class AbstractImportCommand extends AbstractCommand
{
    protected $filePath;
    protected $test;
    protected $error = false;

    protected function __construct(protected TranslatorInterface $translator)
    {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->addOption('file-path', null, InputOption::VALUE_OPTIONAL)
            ->addOption('test', null, InputOption::VALUE_OPTIONAL);
    }

    protected function interact(InputInterface $input, OutputInterface $output)
    {
        $ts = fn($message) => $this->translator->trans($message);
        $this->filePath = $this->askQuestion($ts('import_command.file_csv') . ' : ', $input, $output, $this->filePath);
        $this->test = $this->askConfirmationQuestion($ts('import_command.test'), $input, $output, $this->test);
    }

    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        $this->filePath = $input->getOption('file-path');
        $this->test = (bool) $input->getOption('test');
        if (!$input->isInteractive()) {
            if (null == $this->filePath) {
                $output->writeln($this->translator->trans('import_command.error.filepath_is_mandatory'));
                $this->error = true;
            }
        }
    }

    protected function displayImportResults($results, bool $test, int $importDuration, OutputInterface $output, $ts)
    {
        foreach ($results as $i => $result) {
            $output->writeln($ts('admin.import_result.row') . ' ' . ($i + 1) . ' : ' . $ts('admin.import_result.' . $result));
        }
        if ($test) {
            $output->writeln($ts('admin.import_result.test_message'));
        } else {
            $output->writeln($ts('admin.import_result.success'));
        }
        $output->writeln($ts('admin.import_result.duration') . ' : ' . date('H:i:s', $importDuration));
    }
}