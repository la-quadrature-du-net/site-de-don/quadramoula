<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Console\Question\Question;

abstract class AbstractCommand extends Command
{
    protected function askQuestion($question, InputInterface $input, OutputInterface $output, $default = '')
    {
        $helper = $this->getHelper('question');
        $question = new Question($question, $default);
        return $helper->ask($input, $output, $question);
    }

    protected function askConfirmationQuestion($question, InputInterface $input, OutputInterface $output, $default = true)
    {
        $helper = $this->getHelper('question');
        $question = new ConfirmationQuestion($question, $default);
        return $helper->ask($input, $output, $question);
    }

    protected function askHiddenQuestion($question, InputInterface $input, OutputInterface $output, $default = '')
    {
        $helper = $this->getHelper('question');
        $question = new Question($question, $default);
        $question->setHidden(true);
        $question->setHiddenFallback(false);
        return $helper->ask($input, $output, $question);
    }
}