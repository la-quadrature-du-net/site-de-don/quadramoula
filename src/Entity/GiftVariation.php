<?php

namespace App\Entity;

use App\Repository\GiftVariationRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Contract\Entity\TimestampableInterface;
use Knp\DoctrineBehaviors\Contract\Entity\TranslatableInterface;
use Knp\DoctrineBehaviors\Model\Timestampable\TimestampableTrait;
use Knp\DoctrineBehaviors\Model\Translatable\TranslatableTrait;

#[ORM\Entity(repositoryClass: GiftVariationRepository::class)]
class GiftVariation implements TimestampableInterface, TranslatableInterface
{
    use TimestampableTrait;
    use TranslatableTrait;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'variations')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Gift $gift = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $refImport = null;

    #[ORM\OneToMany(mappedBy: 'giftVariation', targetEntity: Compensation::class)]
    private Collection $compensations;

    public function __construct()
    {
        $this->compensations = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->getName();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->translate()->getName();
    }

    public function getGift(): ?Gift
    {
        return $this->gift;
    }

    public function setGift(?Gift $gift): static
    {
        $this->gift = $gift;

        return $this;
    }

    public function getRefImport(): ?string
    {
        return $this->refImport;
    }

    public function setRefImport(?string $refImport): static
    {
        $this->refImport = $refImport;

        return $this;
    }

    /**
     * @return Collection<int, Compensation>
     */
    public function getCompensations(): Collection
    {
        return $this->compensations;
    }

    public function addCompensation(Compensation $compensation): static
    {
        if (!$this->compensations->contains($compensation)) {
            $this->compensations->add($compensation);
            $compensation->setGift($this);
        }

        return $this;
    }

    public function removeCompensation(Compensation $compensation): static
    {
        if ($this->compensations->removeElement($compensation)) {
            // set the owning side to null (unless already changed)
            if ($compensation->getGift() === $this) {
                $compensation->setGift(null);
            }
        }

        return $this;
    }
}
