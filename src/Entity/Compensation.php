<?php

namespace App\Entity;

use App\Repository\CompensationRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Knp\DoctrineBehaviors\Contract\Entity\TimestampableInterface;
use Knp\DoctrineBehaviors\Model\Timestampable\TimestampableTrait;

#[ORM\Entity(repositoryClass: CompensationRepository::class)]
#[ORM\Index(name: "user_gift_date_idx", columns: ["user_id", "gift_id", "created_at"])]
#[Gedmo\Loggable]
class Compensation implements TimestampableInterface
{
    use TimestampableTrait;

    const STATUS_REQUESTED = 1;
    const STATUS_SENT = 2;
    const STATUS_RETURNED_TO_SENDER = 3;
    const STATUS_CANCELLED = 99;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'compensations')]
    #[ORM\JoinColumn(nullable: true)]
    private ?User $user = null;

    #[ORM\Column]
    #[Gedmo\Versioned]
    private ?int $status = self::STATUS_REQUESTED;

    #[ORM\ManyToOne]
    private ?Gift $gift = null;

    #[ORM\ManyToOne]
    private ?GiftVariation $giftVariation = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    #[Gedmo\Versioned]
    private ?string $comment = null;

    #[ORM\Column]
    private ?bool $keepAddress = false;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): static
    {
        $this->user = $user;

        return $this;
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(int $status): static
    {
        $this->status = $status;

        return $this;
    }

    public function getStatusString(): ?string
    {
        switch ($this->getStatus()) {
            case self::STATUS_REQUESTED:
                return 'compensation.status.requested';
            case self::STATUS_SENT:
                return 'compensation.status.sent';
            case self::STATUS_RETURNED_TO_SENDER:
                return 'compensation.status.returned_to_sender';
            case self::STATUS_CANCELLED:
                return 'compensation.status.cancelled';
            default:
                return null;
        }
    }

    public function getGift(): ?Gift
    {
        return $this->gift;
    }

    public function setGift(?Gift $gift): static
    {
        $this->gift = $gift;

        return $this;
    }

    public function getGiftVariation(): ?GiftVariation
    {
        return $this->giftVariation;
    }

    public function setGiftVariation(?GiftVariation $giftVariation): static
    {
        $this->giftVariation = $giftVariation;

        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(?string $comment): static
    {
        $this->comment = $comment;

        return $this;
    }

    public function getAddress(): ?string
    {
        return null !== $this->getUser() && null !== $this->getUser()->getAddress() ? $this->getUser()->getAddress() : '';
    }

    public function getExportData()
    {
        return [
            'fields.date' => $this->getCreatedAt(),
            'fields.user' => $this->getUser(),
            'fields.address' => $this->getUser()->getAddress(),
            'fields.status' => $this->getStatusString(),
            'fields.gift' => $this->getGift(),
            'fields.gift_variation' => $this->getGiftVariation(),
            'fields.comment' => $this->getComment(),
        ];
    }

    public function isKeepAddress(): ?bool
    {
        return $this->keepAddress;
    }

    public function setKeepAddress(bool $keepAddress): static
    {
        $this->keepAddress = $keepAddress;

        return $this;
    }
}
