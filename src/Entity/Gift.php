<?php

namespace App\Entity;

use App\Repository\GiftRepository;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Contract\Entity\TimestampableInterface;
use Knp\DoctrineBehaviors\Contract\Entity\TranslatableInterface;
use Knp\DoctrineBehaviors\Model\Timestampable\TimestampableTrait;
use Knp\DoctrineBehaviors\Model\Translatable\TranslatableTrait;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

#[ORM\Entity(repositoryClass: GiftRepository::class)]
#[Vich\Uploadable]
class Gift implements TimestampableInterface, TranslatableInterface
{
    use TimestampableTrait;
    use TranslatableTrait;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\OneToMany(mappedBy: 'gift', targetEntity: GiftVariation::class, cascade: ['remove'])]
    private Collection $variations;

    #[ORM\Column]
    private ?int $donationAmountNeeded = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $image = null;

    #[Vich\UploadableField(mapping: 'gift', fileNameProperty: 'image')]
    private $imageFile;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $image2 = null;

    #[Vich\UploadableField(mapping: 'gift', fileNameProperty: 'image2')]
    private $image2File;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $refImport = null;

    #[ORM\OneToMany(mappedBy: 'gift', targetEntity: Compensation::class)]
    private Collection $compensations;

    #[ORM\Column]
    private ?bool $active = true;

    public function __construct()
    {
        $this->variations = new ArrayCollection();
        $this->compensations = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->getName();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->translate()->getName();
    }

    public function getDescription(): ?string
    {
        return $this->translate()->getDescription();
    }

    /**
     * @return Collection<int, GiftVariation>
     */
    public function getVariations(): Collection
    {
        return $this->variations;
    }

    public function addVariation(GiftVariation $variation): static
    {
        if (!$this->variations->contains($variation)) {
            $this->variations->add($variation);
            $variation->setGift($this);
        }

        return $this;
    }

    public function removeVariation(GiftVariation $variation): static
    {
        if ($this->variations->removeElement($variation)) {
            // set the owning side to null (unless already changed)
            if ($variation->getGift() === $this) {
                $variation->setGift(null);
            }
        }

        return $this;
    }

    public function getDonationAmountNeeded(): ?int
    {
        return $this->donationAmountNeeded;
    }

    public function setDonationAmountNeeded(int $donationAmountNeeded): static
    {
        $this->donationAmountNeeded = $donationAmountNeeded;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(?string $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getImageFile()
    {
        return $this->imageFile;
    }

    public function setImageFile($imageFile)
    {
        $this->imageFile = $imageFile;
        if ($imageFile) {
            $this->setUpdatedAt(new DateTime());
        }
        return $this;
    }

    public function getImage2(): ?string
    {
        return $this->image2;
    }

    public function setImage2(?string $image2): self
    {
        $this->image2 = $image2;

        return $this;
    }

    public function getImage2File()
    {
        return $this->image2File;
    }

    public function setImage2File($image2File)
    {
        $this->image2File = $image2File;
        if ($image2File) {
            $this->setUpdatedAt(new DateTime());
        }
        return $this;
    }

    public function getRefImport(): ?string
    {
        return $this->refImport;
    }

    public function setRefImport(?string $refImport): static
    {
        $this->refImport = $refImport;

        return $this;
    }

    /**
     * @return Collection<int, Compensation>
     */
    public function getCompensations(): Collection
    {
        return $this->compensations;
    }

    public function addCompensation(Compensation $compensation): static
    {
        if (!$this->compensations->contains($compensation)) {
            $this->compensations->add($compensation);
            $compensation->setGift($this);
        }

        return $this;
    }

    public function removeCompensation(Compensation $compensation): static
    {
        if ($this->compensations->removeElement($compensation)) {
            // set the owning side to null (unless already changed)
            if ($compensation->getGift() === $this) {
                $compensation->setGift(null);
            }
        }

        return $this;
    }

    public function isActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(bool $active): static
    {
        $this->active = $active;

        return $this;
    }
}
