<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Contract\Entity\TranslatableInterface;
use Knp\DoctrineBehaviors\Model\Translatable\TranslatableTrait;

#[ORM\Table(name: 'cms')]
#[ORM\Entity(repositoryClass: 'App\Repository\CmsRepository')]
class Cms implements TranslatableInterface
{
    use TranslatableTrait;

    #[ORM\Column(name: 'id', type: 'integer')]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'AUTO')]
    private $id;

    public function getId()
    {
        return $this->id;
    }

    public function getTitle()
    {
        return $this->translate()->getTitle();
    }

    public function getContent()
    {
        return $this->translate()->getContent();
    }

    public function getSlug()
    {
        return $this->translate()->getSlug();
    }
}
