<?php

namespace App\Form\Admin;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;

class ImportBankDonationFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('file', FileType::class, [
                'label' => 'admin.crud.donation.import.file',
            ])
            ->add('test', CheckboxType::class, [
                'label' => 'admin.crud.donation.import.test',
                'required' => false,
                'data' => true,
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'admin.crud.donation.import.submit',
            ]);
    }
}