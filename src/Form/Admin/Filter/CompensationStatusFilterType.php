<?php

namespace App\Form\Admin\Filter;

use App\Entity\Compensation;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CompensationStatusFilterType extends AbstractType
{
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'choices' => [
                'compensation.status.requested' => Compensation::STATUS_REQUESTED,
                'compensation.status.sent' => Compensation::STATUS_SENT,
                'compensation.status.returned_to_sender' => Compensation::STATUS_RETURNED_TO_SENDER,
                'compensation.status.cancelled' => Compensation::STATUS_CANCELLED,
            ],
        ]);
    }

    public function getParent()
    {
        return ChoiceType::class;
    }
}