<?php

namespace App\Form\Admin\Filter;

use App\Entity\Donation;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DonationStatusFilterType extends AbstractType
{
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'choices' => [
                'donation.status.punctual_not_valid' => Donation::STATUS_PUNCTUAL_NOT_VALID,
                'donation.status.punctual_valid' => Donation::STATUS_PUNCTUAL_VALID,
                'donation.status.in_progress' => Donation::STATUS_IN_PROGRESS,
                'donation.status.monthly_not_valid' => Donation::STATUS_MONTHLY_NOT_VALID,
                'donation.status.monthly_valid' => Donation::STATUS_MONTHLY_VALID,
                'donation.status.monthly_handed' => Donation::STATUS_MONTHLY_HANDED,
                //'donation.status.monthly_cancelled' => Donation::STATUS_MONTHLY_CANCELLED,
            ],
        ]);
    }

    public function getParent()
    {
        return ChoiceType::class;
    }
}