<?php

namespace App\Form\Admin\Filter;

use App\Entity\Donation;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PaymentMethodFilterType extends AbstractType
{
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'choices' => [
                'donation.payment_method.cb' => Donation::PAYMENT_METHOD_CB,
                'donation.payment_method.check' => Donation::PAYMENT_METHOD_CHECK,
                'donation.payment_method.bankwire' => Donation::PAYMENT_METHOD_BANKWIRE,
                'donation.payment_method.cash' => Donation::PAYMENT_METHOD_CASH,
            ],
        ]);
    }

    public function getParent()
    {
        return ChoiceType::class;
    }
}