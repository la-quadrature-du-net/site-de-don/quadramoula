<?php

namespace App\Form\Front;

use App\Entity\Address;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AddressFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('firstname', TextType::class, [
                'label' => 'fields.firstname',
            ])
            ->add('lastname', TextType::class, [
                'label' => 'fields.lastname',
            ])
            ->add('address1', TextType::class, [
                'label' => 'fields.address1',
            ])
            ->add('address2', TextType::class, [
                'label' => 'fields.address2',
                'required' => false,
            ])
            ->add('postcode', TextType::class, [
                'label' => 'fields.postcode',
            ])
            ->add('city', TextType::class, [
                'label' => 'fields.city',
            ])
            ->add('state', TextType::class, [
                'label' => 'fields.state',
                'required' => false,
            ])
            ->add('country', TextType::class, [
                'label' => 'fields.country',
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'front.form.user.account.submit',
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Address::class,
        ]);
    }
}
