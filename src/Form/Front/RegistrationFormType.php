<?php

namespace App\Form\Front;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Validator\Constraints\IsTrue;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Contracts\Translation\TranslatorInterface;

class RegistrationFormType extends AbstractType
{
    public function __construct(
        private TranslatorInterface $translator,
        private UrlGeneratorInterface $router,
    )
    {
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $termsText = $this->translator->trans('fields.accept_terms')
                . ' <a target="_blank" href="' . $this->router->generate('cms', ['slug' => $this->translator->trans('fields.accept_terms_link_slug')]) . '">'
            . $this->translator->trans('fields.accept_terms_link_anchor')
            . '</a>';
        $builder
            ->add('email', EmailType::class, [
                'label' => 'fields.email',
            ])
            ->add('plainPassword', RepeatedType::class, [
                'type' => PasswordType::class,
                'options' => [
                    'attr' => [
                        'autocomplete' => 'new-password',
                    ],
                ],
                'first_options' => [
                    'constraints' => [
                        new NotBlank([
                            'message' => $this->translator->trans('front.form.user.register.errors.password.blank'),
                        ]),
                        new Length([
                            'min' => 6,
                            'minMessage' => $this->translator->trans('front.form.user.register.errors.password.length'),
                            // max length allowed by Symfony for security reasons
                            'max' => 4096,
                        ]),
                    ],
                    'label' => 'fields.password',
                ],
                'second_options' => [
                    'label' => 'fields.password_confirmation',
                ],
                'invalid_message' => 'passwords_must_match.message',
                // Instead of being set onto the object directly,
                // this is read and encoded in the controller
                'mapped' => false,
            ])
            ->add('agreeTerms', CheckboxType::class, [
                'label' => $termsText,
                'label_html' => true,
                'mapped' => false,
                'constraints' => [
                    new IsTrue([
                        'message' => 'front.form.user.register.errors.terms',
                    ]),
                ],
            ])->add('submit', SubmitType::class, [
                'label' => 'front.form.user.register.submit',
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
