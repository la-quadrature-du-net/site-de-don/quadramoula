<?php

namespace App\Controller\Front\User;

use App\Entity\Compensation;
use App\Entity\User;
use App\Repository\GiftRepository;
use App\Repository\GiftVariationRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

#[Route('/{_locale<%app.locales%>}/user/compensation')]
class CompensationController extends AbstractController
{
    #[Route('', name: 'user_compensation')]
    public function compensation(
        Request                 $request,
        TranslatorInterface     $translator,
        GiftRepository          $giftRepository,
        GiftVariationRepository $giftVariationRepository,
        EntityManagerInterface  $entityManager
    )
    {
        if ($request->getMethod() == 'POST' && ($idGift = $request->get('gift'))) {
            /** @var User $user */
            $user = $this->getUser();
            $gift = $giftRepository->find($idGift);
            if ($gift->getDonationAmountNeeded() > $user->getCumul()) {
                $this->addFlash('danger', $translator->trans('front.page.user_compensation.insufficient_cumul'));
            } else {
                $compensation = new Compensation();
                $compensation->setUser($this->getUser());
                $compensation->setGift($gift);
                if ($idVariation = $request->get('variation_' . $idGift)) {
                    $giftVariation = $giftVariationRepository->find($idVariation);
                    $compensation->setGiftVariation($giftVariation);
                }
                $compensation->setComment($request->get('comment'));
                $compensation->setKeepAddress((bool)$request->get('keep_address'));
                $entityManager->persist($compensation);
                $user->setCumul($user->getCumul() - $gift->getDonationAmountNeeded());
                $entityManager->flush();
                $this->addFlash('success', $translator->trans('front.page.user_compensation.success'));
            }
        }
        return $this->render('front/user/compensation.html.twig', [
            'bodyClass' => 'compensation',
            'gifts' => $giftRepository->findActives(),
        ]);
    }
}