<?php

namespace App\Controller\Front\User;

use App\Entity\User;
use App\Form\Front\AccountFormType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\HeaderUtils;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

#[Route('/{_locale<%app.locales%>}/user/account')]
class AccountController extends AbstractController
{
    public function __construct(
        private UserPasswordHasherInterface $passwordHasher,
        private EntityManagerInterface      $entityManager,
        private TranslatorInterface         $translator,
        private Security                    $security,
    )
    {
    }

    #[Route('', name: 'user_account')]
    public function account(Request $request)
    {
        /** @var User $user */
        $user = $this->getUser();
        $form = $this->createForm(AccountFormType::class, $user);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            if ('' != ($password = $form->get('plainPassword')->getData())) {
                $encodedPassword = $this->passwordHasher->hashPassword(
                    $user,
                    $password
                );
                $user->setPassword($encodedPassword);
            }
            $this->entityManager->flush();
            $this->addFlash('success', $this->translator->trans('front.form.user.account.success'));
        }
        $this->entityManager->refresh($user);
        return $this->render('front/user/account.html.twig', [
            'accountForm' => $form->createView(),
        ]);
    }

    #[Route('/download-personal-data', name: 'user_account_download_personal_data')]
    public function downloadPersonalData()
    {
        $response = new JsonResponse();
        /** @var User $user */
        $user = $this->getUser();
        $data = [
            'createdAt' => $user->getCreatedAt()->format('Y-m-d H:i:s'),
            'email' => $user->getEmail(),
            'total' => $user->getTotal(),
            'cumul' => $user->getCumul(),
        ];
        if ($address = $user->getAddress()) {
            $data['firstname'] = $address->getFirstname();
            $data['lastfirstname'] = $address->getLastname();
            $data['address1'] = $address->getAddress1();
            $data['address2'] = $address->getAddress2();
            $data['postcode'] = $address->getPostcode();
            $data['city'] = $address->getCity();
            $data['state'] = $address->getState();
            $data['country'] = $address->getCountry();
        }
        $donations = [];
        $compensations = [];
        foreach ($user->getDonations() as $donation) {
            $donations[] = [
                'createdAt' => $donation->getCreatedAt()->format('Y-m-d H:i:s'),
                'amount' => $donation->getAmount(),
                'status' => $this->translator->trans($donation->getStatusString()),
                'paymentMethod' => $this->translator->trans($donation->getPaymentMethodString()),
                'transaction' => $donation->getTransaction(),
            ];
        }
        if (count($donations) > 0) {
            $data['donations'] = $donations;
        }
        foreach ($user->getCompensations() as $compensation) {
            $compensations = [
                'createdAt' => $compensation->getCreatedAt()->format('Y-m-d H:i:s'),
                'gift' => $compensation->getGift()->getName(),
                'giftVariation' => $compensation->getGiftVariation()->getName(),
                'status' => $this->translator->trans($compensation->getStatusString()),
            ];
        }
        if (count($compensations) > 0) {
            $data['compensations'] = $compensations;
        }
        $disposition = HeaderUtils::makeDisposition(
            HeaderUtils::DISPOSITION_ATTACHMENT,
            'personal-data.json'
        );
        $response->setData($data);
        $response->setEncodingOptions($response->getEncodingOptions() | JSON_PRETTY_PRINT);
        $response->headers->set('Content-Disposition', $disposition);
        return $response;
    }

    #[Route('/delete-account', name: 'user_account_delete_account')]
    public function deleteAccount(Request $request): RedirectResponse
    {
        /** @var User $user */
        $user = $this->getUser();
        foreach ($user->getDonations() as $donation) {
            $donation->setUser(null);
        }
        foreach ($user->getCompensations() as $compensation) {
            $compensation->setUser(null);
        }
        foreach ($user->getUserBankAliases() as $userBankAlias) {
            $userBankAlias->setUser(null);
        }
        $this->entityManager->remove($user);
        $this->entityManager->flush();
        $this->security->logout(false);
        $this->addFlash('success', $this->translator->trans('front.page.user_account.delete_account_success'));
        return $this->redirectToRoute('index');
    }
}