<?php

namespace App\Controller\Front\User;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/{_locale<%app.locales%>}/user')]
class DashboardController extends AbstractController
{
    #[Route('', name: 'user_dashboard')]
    public function dashboard()
    {
        return $this->render('front/user/dashboard.html.twig', [
            'canonicalUrl' => $this->generateUrl('user_dashboard'),
        ]);
    }
}