<?php

namespace App\Controller\Front\User;

use App\Entity\Address;
use App\Entity\User;
use App\Form\Front\AddressFormType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

#[Route('/{_locale<%app.locales%>}/user/address')]
class AddressController extends AbstractController
{
    #[Route('', name: 'user_address')]
    public function address(Request $request, EntityManagerInterface $entityManager, TranslatorInterface $translator)
    {
        /** @var User $user */
        $user = $this->getUser();
        $address = $user->getAddress();
        if (null == $address) {
            $address = new Address();
            $address->setUser($user);
        }
        $form = $this->createForm(AddressFormType::class, $address);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($address);
            $entityManager->flush();
            $this->addFlash('success', $translator->trans('front.form.user.account.success'));
        }
        $entityManager->refresh($user);
        return $this->render('front/user/address.html.twig', [
            'addressForm' => $form->createView(),
        ]);
    }
}