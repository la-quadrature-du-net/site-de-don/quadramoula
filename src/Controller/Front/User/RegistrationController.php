<?php

namespace App\Controller\Front\User;

use App\Entity\User;
use App\Form\Front\RegistrationFormType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

#[Route('/{_locale<%app.locales%>}/user')]
class RegistrationController extends AbstractController
{
    #[Route('/register', name: 'user_register')]
    public function register(Request $request, UserPasswordHasherInterface $userPasswordHasher, EntityManagerInterface $entityManager): Response
    {
        $user = new User();
        $redirectUrl = $request->get('redirect');
        $form = $this->createForm(RegistrationFormType::class, $user,
            [
                'action' => $this->generateUrl('user_register') . ($redirectUrl ? '?redirect=' . $redirectUrl : ''),
            ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // encode the plain password
            $user->setPassword(
                $userPasswordHasher->hashPassword(
                    $user,
                    $form->get('plainPassword')->getData()
                )
            );

            $entityManager->persist($user);
            $entityManager->flush();

            $token = new UsernamePasswordToken($user, 'user', $user->getRoles());
            $this->container->get('security.token_storage')->setToken($token);
            if ($redirectUrl) {
                return $this->redirect($redirectUrl);
            }
            else {
                return $this->redirectToRoute('user_dashboard');
            }
        }

        return $this->render('front/user/register.html.twig', [
            'bodyClass' => 'register',
            'registrationForm' => $form->createView(),
        ]);
    }
}
