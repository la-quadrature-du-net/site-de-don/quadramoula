<?php

namespace App\Controller\Front;

use App\Entity\CmsTranslation;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class CmsController extends AbstractController
{
    #[Route("/{_locale<%app.locales%>}/cms/{slug}", name: "cms")]
    public function cms(CmsTranslation $cmsTranslation)
    {
        return $this->render('front/cms.html.twig', [
            'bodyClass' => 'cms',
            'cms' => $cmsTranslation,
        ]);
    }
}
