<?php

namespace App\Controller\Front;

use App\Repository\FaqRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class FaqController extends AbstractController
{
    public function __construct(private FaqRepository $faqRepository)
    {
    }

    #[Route('/{_locale<%app.locales%>}/faq', name: 'faq')]
    public function index()
    {
        return $this->render('front/faq.html.twig', [
            'bodyClass' => 'faq',
            'faqs' => $this->faqRepository->findAll(),
        ]);
    }
}