<?php

namespace App\Controller\Front;

use App\Repository\GiftRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Annotation\Route;

class IndexController extends AbstractController
{
    #[Route('/', name: 'index_root')]
    public function indexNoLocale(): RedirectResponse
    {
        return $this->redirectToRoute('index', ['_locale' => 'fr']);
    }

    #[Route('/{_locale<%app.locales%>}', name: 'index')]
    public function index(GiftRepository $giftRepository)
    {
        return $this->render('front/index.html.twig', [
            'bodyClass' => 'index',
            'gifts' => $giftRepository->findActives(),
        ]);
    }
}