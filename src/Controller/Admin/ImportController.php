<?php

namespace App\Controller\Admin;

use App\Form\Admin\ImportCompensationFormType;
use App\Form\Admin\ImportDonationFormType;
use App\Form\Admin\ImportUserFormType;
use App\Service\Import\CompensationCsvImportService;
use App\Service\Import\DonationCsvImportService;
use App\Service\Import\UserCsvImportService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ImportController extends AbstractController
{
    #[Route("/admin/import", name: 'admin_import')]
    public function import(
        Request                      $request,
        UserCsvImportService         $userCsvImportService,
        DonationCsvImportService     $donationCsvImportService,
        CompensationCsvImportService $compensationCsvImportService
    ): Response
    {
        $formUser = $this->createForm(ImportUserFormType::class);
        $formUser->handleRequest($request);
        $formDonation = $this->createForm(ImportDonationFormType::class);
        $formDonation->handleRequest($request);
        $formCompensation = $this->createForm(ImportCompensationFormType::class);
        $formCompensation->handleRequest($request);
        $importStart = time();
        if ($formUser->isSubmitted() && $formUser->isValid()) {
            $formData = $formUser->getData();
            $test = $formData['test'];
            $csvData = $userCsvImportService->getCsvData($formUser['file']->getData()->getPathname());
            $results = $userCsvImportService->processImport($csvData, $test);
        } elseif ($formDonation->isSubmitted() && $formDonation->isValid()) {
            $formData = $formDonation->getData();
            $test = $formData['test'];
            $csvData = $donationCsvImportService->getCsvData($formDonation['file']->getData()->getPathname());
            $results = $donationCsvImportService->processImport($csvData, $test);
        } elseif ($formCompensation->isSubmitted() && $formCompensation->isValid()) {
            $formData = $formCompensation->getData();
            $test = $formData['test'];
            $csvData = $compensationCsvImportService->getCsvData($formCompensation['file']->getData()->getPathname());
            $results = $compensationCsvImportService->processImport($csvData, $test);
        }
        $importDuration = time() - $importStart;
        return $this->render('admin/page/import.html.twig', [
            'results' => isset($results) ? $results : null,
            'test' => isset($test) ? $test : null,
            'formUser' => $formUser->createView(),
            'formDonation' => $formDonation->createView(),
            'formCompensation' => $formCompensation->createView(),
            'importDuration' => date('H:i:s', $importDuration),
        ]);
    }
}