<?php

namespace App\Controller\Admin\Crud;

use App\Entity\Gift;
use App\Field\TranslationField;
use EasyCorp\Bundle\EasyAdminBundle\Config\Asset;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\Field;
use EasyCorp\Bundle\EasyAdminBundle\Field\NumberField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Vich\UploaderBundle\Form\Type\VichFileType;

class GiftCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Gift::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setEntityLabelInSingular('admin.crud.gift.entity.label.singular')
            ->setEntityLabelInPlural('admin.crud.gift.entity.label.plural')
            ->showEntityActionsInlined()
            ->setFormThemes([
                '@FOSCKEditor/Form/ckeditor_widget.html.twig',
                '@EasyAdmin/crud/form_theme.html.twig',
                '@A2lixTranslationForm/bootstrap_5_layout.html.twig',
            ]);
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            TranslationField::new('translations', 'translations', [
                'name' => [
                    'field_type' => TextType::class,
                    'required' => true,
                    'label' => 'fields.name',
                ],
                'description' => [
                    'field_type' => TextType::class,
                    'label' => 'fields.description',
                ],
            ])->onlyOnForms(),
            TextField::new('name', 'fields.name')->hideOnForm(),
            AssociationField::new('variations', 'fields.variations')->hideOnIndex(),
            NumberField::new('donation_amount_needed', 'fields.donation_amount_needed'),
            BooleanField::new('active', 'fields.active'),
            Field::new('imageFile')
                ->setFormType(VichFileType::class)
                ->addJsFiles(Asset::fromEasyAdminAssetPackage('field-image.js'))
                ->setLabel('fields.image')
                ->onlyOnForms(),
            Field::new('image2File')
                ->setFormType(VichFileType::class)
                ->addJsFiles(Asset::fromEasyAdminAssetPackage('field-image.js'))
                ->setLabel('fields.image2')
                ->onlyOnForms(),
        ];
    }
}
