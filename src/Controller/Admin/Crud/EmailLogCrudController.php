<?php

namespace App\Controller\Admin\Crud;

use App\Entity\EmailLog;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class EmailLogCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return EmailLog::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setEntityLabelInSingular('admin.crud.email_log.entity.label.singular')
            ->setEntityLabelInPlural('admin.crud.email_log.entity.label.plural')
            ->showEntityActionsInlined()
            ->setDefaultSort(['id' => 'DESC']);
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            ChoiceField::new('type', 'fields.type')->setChoices([
                'entities.admin_user' => EmailLog::TYPE_ADMIN,
                'entities.user' => EmailLog::TYPE_USER,
            ])->renderExpanded()->addCssClass('type-wrapper'),
            TextField::new('recipient', 'fields.recipient'),
            TextField::new('subject', 'fields.subject')->onlyOnIndex(),
            DatetimeField::new('sentAt', 'fields.sent_at'),
            TextField::new('body', 'fields.message')
                ->setTemplatePath('admin/fields_display/text-html.html.twig')
                ->onlyOnDetail(),
        ];
    }

    public function configureActions(Actions $actions): Actions
    {
        return $actions
            ->remove(Crud::PAGE_INDEX, Action::DELETE)
            ->remove(Crud::PAGE_INDEX, Action::NEW)
            ->remove(Crud::PAGE_INDEX, Action::EDIT)
            ->add(Crud::PAGE_INDEX, Action::DETAIL);
    }
}
