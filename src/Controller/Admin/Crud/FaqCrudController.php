<?php

namespace App\Controller\Admin\Crud;

use App\Entity\Faq;
use App\Field\TranslationField;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class FaqCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Faq::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setEntityLabelInSingular('admin.crud.faq.entity.label.singular')
            ->setEntityLabelInPlural('admin.crud.faq.entity.label.plural')
            ->showEntityActionsInlined()
            ->setFormThemes([
                '@FOSCKEditor/Form/ckeditor_widget.html.twig',
                '@EasyAdmin/crud/form_theme.html.twig',
                '@A2lixTranslationForm/bootstrap_5_layout.html.twig',
            ]);
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('question', 'fields.question')->hideOnForm(),
            TextField::new('anchor', 'fields.anchor'),
            TranslationField::new('translations', 'translations', [
                'question' => [
                    'field_type' => TextType::class,
                    'required' => true,
                    'label' => 'fields.question',
                ],
                'response' => [
                    'field_type' => CKEditorType::class,
                    'required' => true,
                    'label' => 'fields.response',
                ],
            ])->onlyOnForms(),
        ];
    }
}
