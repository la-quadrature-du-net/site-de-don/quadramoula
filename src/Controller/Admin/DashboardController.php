<?php

namespace App\Controller\Admin;

use App\Entity\AdminUser;
use App\Entity\Cms;
use App\Entity\Compensation;
use App\Entity\Donation;
use App\Entity\EmailLog;
use App\Entity\Faq;
use App\Entity\Gift;
use App\Entity\GiftVariation;
use App\Entity\User;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AbstractDashboardController
{
    #[Route('/admin', name: 'admin')]
    public function index(): Response
    {
        return $this->render('admin/dashboard.html.twig', []);
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('<img src="/images/logo.png" alt="Quadramoula" />')
            ->setFaviconPath('/images/favicon.ico');
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linkToDashboard('admin.menu.dashboard', 'fa fa-bookmark-o');
        yield MenuItem::section('admin.menu.donations_section.title');
        yield MenuItem::linkToCrud('admin.menu.donations_section.users', 'fa fa-user', User::class);
        yield MenuItem::linkToCrud('admin.menu.donations_section.donations', 'fa fa-hand-holding-dollar', Donation::class);
        yield MenuItem::linkToCrud('admin.menu.donations_section.compensations', 'fa fa-gifts', Compensation::class);
        yield MenuItem::section('admin.menu.settings_section.title');
        yield MenuItem::linkToCrud('admin.menu.settings_section.gifts', 'fa fa-gift', Gift::class);
        yield MenuItem::linkToCrud('admin.menu.settings_section.gifts_variations', 'fa fa-tags', GiftVariation::class);
        yield MenuItem::section('admin.menu.content_section.title');
        yield MenuItem::linkToCrud('admin.menu.content_section.faq', 'fa fa-question', Faq::class);
        yield MenuItem::linkToCrud('admin.menu.content_section.cms', 'fa fa-file-lines', Cms::class);
        yield MenuItem::section('admin.menu.admin_section.title');
        yield MenuItem::linkToCrud('admin.menu.admin_section.users', 'fa fa-user-secret', AdminUser::class);
        yield MenuItem::linkToCrud('admin.menu.admin_section.email_log', 'fa fa-envelope', EmailLog::class);
        yield MenuItem::linktoRoute('admin.menu.admin_section.import_data', 'fa fa-upload', 'admin_import');
        yield MenuItem::section()->setCssClass('no-title');
        yield MenuItem::linkToLogout('admin.menu.logout', 'fa fa-right-from-bracket');
    }

    public function configureCrud(): Crud
    {
        return Crud::new()
            ->overrideTemplates([
                'layout' => 'admin/layout.html.twig',
            ]);
    }
}
