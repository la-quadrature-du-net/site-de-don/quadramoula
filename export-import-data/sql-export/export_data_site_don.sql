use soutien;

SELECT
    users.email,
    contreparties.status,
    quoi,
    taille,
    datec
FROM contreparties
LEFT JOIN users
ON contreparties.user_id = users.id
INTO OUTFILE '/tmp/contreparties.csv'
FIELDS TERMINATED BY ';'
ENCLOSED BY '"'
LINES TERMINATED BY '\n';

SELECT
    users.email,
    dons.status,
    lang AS locale,
    somme AS amount,
    1 AS payment_method,
    datec AS date,
    identifier AS bank_alias,
    dons.id AS transaction
FROM dons
LEFT JOIN users
ON dons.user_id = users.id
INTO OUTFILE '/tmp/dons.csv'
FIELDS TERMINATED BY ';'
ENCLOSED BY '"'
LINES TERMINATED BY '\n';

SELECT
    email,
    total,
    cumul,
    commentaire,
    pseudo,
    pseudo,
    adresses.adresse,
    adresses.adresse2,
    adresses.codepostal,
    adresses.ville,
    adresses.etat,
    adresses.pays
FROM users
LEFT JOIN adresses
ON  users.id = adresses.user_id
INTO OUTFILE '/tmp/adresses_and_users.csv'
FIELDS TERMINATED BY ';'
ENCLOSED BY '"'
LINES TERMINATED BY '\n';
