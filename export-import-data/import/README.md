For import, you need to visit the /admin page on Quadramoula and select the import options.

You can also import the data from the command-line.

For example; `php bin/console app:import:user` 
