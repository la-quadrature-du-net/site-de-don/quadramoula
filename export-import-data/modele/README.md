# Modèle des données d'import de Quadramoula

Ces fichiers CSV décrivent la façon dont les fichiers CSV doivent être formatés pour l'import des données.

---

# Data model for importing into Quadramoula

Theses CSV files describe the way the data must be formatted for data import.
